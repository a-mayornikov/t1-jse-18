package ru.t1.mayornikov.tm.exception.field;

public final class NameEmptyException extends AbstractFieldException{

    public NameEmptyException() {
        super("Name is empty...");
    }
    
}