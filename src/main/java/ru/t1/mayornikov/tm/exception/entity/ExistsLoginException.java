package ru.t1.mayornikov.tm.exception.entity;

public final class ExistsLoginException extends AbstractEntityException{

    public ExistsLoginException() {
        super("No one User use this login...");
    }

}