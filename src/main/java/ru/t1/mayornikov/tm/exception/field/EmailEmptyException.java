package ru.t1.mayornikov.tm.exception.field;

public final class EmailEmptyException extends AbstractFieldException{

    public EmailEmptyException() {
        super("Email is empty...");
    }
    
}