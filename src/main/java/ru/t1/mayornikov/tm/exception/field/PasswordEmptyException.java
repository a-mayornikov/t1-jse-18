package ru.t1.mayornikov.tm.exception.field;

public final class PasswordEmptyException extends AbstractFieldException{

    public PasswordEmptyException() {
        super("Password is empty...");
    }
    
}