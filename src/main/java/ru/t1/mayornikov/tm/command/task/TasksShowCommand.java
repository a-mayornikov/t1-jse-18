package ru.t1.mayornikov.tm.command.task;

public class TasksShowCommand extends AbstractTaskCommand{

    private final static String NAME = "task-list";

    private final static String DESCRIPTION = "Show tasks.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW TASKS]");
        getTaskService().renderTasks();
    }

}