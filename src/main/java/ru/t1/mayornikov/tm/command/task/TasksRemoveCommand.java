package ru.t1.mayornikov.tm.command.task;

import ru.t1.mayornikov.tm.util.TerminalUtil;

public class TasksRemoveCommand extends AbstractTaskCommand{

    private final static String NAME = "task-clear";

    private final static String DESCRIPTION = "Remove all tasks.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[CLEAR TASKS]");
        getTaskService().clear();
    }

}