package ru.t1.mayornikov.tm.command.project;

import ru.t1.mayornikov.tm.model.Project;
import ru.t1.mayornikov.tm.util.TerminalUtil;

public final class ProjectShowByIndexCommand extends AbstractProjectCommand {

    private static final String NAME = "project-show-by-id";

    private static final String DESCRIPTION = "Show project by id.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Project project = getProjectService().findOne(TerminalUtil.nextNumber() - 1);
        getProjectService().showProject(project);
    }

}