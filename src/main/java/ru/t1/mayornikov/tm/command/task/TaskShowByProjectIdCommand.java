package ru.t1.mayornikov.tm.command.task;

import ru.t1.mayornikov.tm.model.Task;
import ru.t1.mayornikov.tm.util.TerminalUtil;

import java.util.List;

public class TaskShowByProjectIdCommand extends AbstractTaskCommand{

    private final static String NAME = "task-show-by-id";

    private final static String DESCRIPTION = "Show task by id.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[TASK LIST BY PROJECT ID]");
        System.out.println("ENTER PROJECT ID:");
        final List<Task> tasks = getTaskService().findAllByProjectId(TerminalUtil.nextLine());
        getTaskService().renderTasks(tasks);
    }

}