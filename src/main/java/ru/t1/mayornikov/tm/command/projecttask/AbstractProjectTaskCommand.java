package ru.t1.mayornikov.tm.command.projecttask;

import ru.t1.mayornikov.tm.api.service.IProjectTaskService;
import ru.t1.mayornikov.tm.command.AbstractCommand;

public abstract class AbstractProjectTaskCommand extends AbstractCommand {

    protected IProjectTaskService getProjectTaskService() {
        return getServiceLocator().getProjectTaskService();
    }

    @Override
    public String getArgument() {
        return null;
    }

}