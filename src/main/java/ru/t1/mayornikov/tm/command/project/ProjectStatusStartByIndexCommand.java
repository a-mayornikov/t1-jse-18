package ru.t1.mayornikov.tm.command.project;

import ru.t1.mayornikov.tm.enumerated.Status;
import ru.t1.mayornikov.tm.util.TerminalUtil;

public class ProjectStatusStartByIndexCommand extends AbstractProjectCommand{

    private static final String NAME = "project-start-by-index";

    private static final String DESCRIPTION = "Set status project to started by index.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[START PROJECT]");
        System.out.println("ENTER PROJECT INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        getProjectService().changeProjectStatus(index, Status.IN_PROGRESS);
    }

}